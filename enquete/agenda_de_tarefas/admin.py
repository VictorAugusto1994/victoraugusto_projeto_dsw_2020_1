from django.contrib import admin
from agenda_de_tarefas import models

#from . models import Usuario, Tarefa, TipoDeTarefa
'''
class TarefaAdmin(admin.ModelAdmin):
    list_display = ('nome', 'descricao', 'dia', 'mostrar')
    list_display_links = ('nome')
    list_filter = ('nome')
    list_per_page = 10
    search_fields = ('nome')
    list_editable = ('mostrar')
'''

admin.site.register(models.Usuario)
admin.site.register(models.Tarefa)
admin.site.register(models.TipoDeTarefa)