from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib import auth, messages
from django.core.paginator import Paginator
from django.db.models import Q
from agenda_de_tarefas import models
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

@login_required(redirect_field_name='root')
def tarefas(request):
    tarefas = models.Tarefa.objects.all().filter(
        mostrar = True
    )
    paginator = Paginator(tarefas, 3)

    page = request.GET.get('p')
    tarefas = paginator.get_page(page)

    return render(request, 'agenda_de_tarefas/tarefas.html', {'tarefas':tarefas,})

#================================================
@login_required(redirect_field_name='root')
def detalhe_tarefa(request, tarefa_id):
    detalhetarefa = get_object_or_404(models.Tarefa, id = tarefa_id)

    if not detalhetarefa.mostrar:
        raise Http404()
    #return HttpResponseRedirect(reverse('agenda_de_tarefas:detalhe_tarefa'))
    return render(request, 'agenda_de_tarefas/detalhes_tarefa.html', {'detalhetarefa':detalhetarefa,})

#================================================
@login_required(redirect_field_name='root')
def busca(request):
    termo = request.GET.get('termo')

    tarefas = models.Tarefa.objects.order_by('-id').filter( Q(nome__icontains = termo), mostrar = True)
    paginator = Paginator(tarefas, 4)

    page = request.GET.get('p')
    tarefas = paginator.get_page(page)

    return render(request, 'agenda_de_tarefas/buscar_tarefas.html', {'tarefas':tarefas})

#================================================

def entrar(request):
    if request.method != "POST":
        return render(request, 'agenda_de_tarefas/index.html')

    nome = request.POST.get('usuario')
    senha = request.POST.get('senha')
    user = authenticate( username = nome, password = senha)

    if user == None:
        return render(request, 'agenda_de_tarefas/index.html')
    else:
        auth.login(request, user = user)
        return HttpResponseRedirect(reverse('agenda_de_tarefas:tarefas'))

#================================================

def sair(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('agenda_de_tarefas:root'))

