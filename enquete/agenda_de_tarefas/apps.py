from django.apps import AppConfig


class AgendaDeTarefasConfig(AppConfig):
    name = 'agenda_de_tarefas'
