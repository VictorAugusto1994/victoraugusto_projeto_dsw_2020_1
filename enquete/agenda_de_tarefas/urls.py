from django.urls import path
from . import views

app_name ='agenda_de_tarefas'

urlpatterns = [
    path('', views.entrar, name='root'),

    path('sair/', views.sair, name='sair'),

    path('tarefas/', views.tarefas, name='tarefas'),

    path('detalhe_tarefa/<int:tarefa_id>/', views.detalhe_tarefa, name='detalhe_tarefa'),

    path('busca/', views.busca, name='busca'),

]