from django.db import models
from django.db.models.deletion import DO_NOTHING
from django.utils import timezone
from django.contrib.auth.models import User


class TipoDeTarefa(models.Model):
    tipo_de_tarefa = models.CharField(
        default='T',
        max_length=1000,
        choices=(
            ("Trabalho", "Trabalho"),
            ("Casa", "Casa"),
            ("Pessoal", "Pessoal"),
            ("Outros", "Outros"),
        ),
        blank=False, null=False
    )

    def __str__(self):
        return self.tipo_de_tarefa

class Usuario(models.Model):
    usuario = models.OneToOneField(User, on_delete=DO_NOTHING, verbose_name="usuario")

    def __str__(self):
        return self.usuario

class Tarefa(models.Model):
    nome = models.CharField(max_length=100)
    descricao = models.TextField(max_length=100)
    dia = models.DateTimeField(default=timezone.now)
    mostrar = models.BooleanField(default=True)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    tipotarefa = models.OneToOneField(TipoDeTarefa, on_delete=models.CASCADE)

    def __str__(self):
        return self.nome







