from django.db import models
from django.utils import timezone
import datetime

class Pergunta(models.Model):
    texto = models.CharField(max_length=200)
    data_publicacao = models.DateTimeField('Data de Publicação')
    def __str__(self):
        return self.texto
    def publicada_recentemente(self):
        agora = timezone.now()
        return agora - datetime.timedelta(days=1) <= self.data_publicacao <= agora
    publicada_recentemente.admin_order_field = 'data_publicacao'
    publicada_recentemente.boolean = True
    publicada_recentemente.short_description = 'ultimas 24hs?'

class Opcao(models.Model):
    texto = models.CharField(max_length=200)
    votos = models.IntegerField(default=0)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    def __str__(self):
        return self.texto

class Comentario(models.Model):
    autor_comentario = models.CharField(max_length=150)
    texto = models.CharField(max_length=200)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    data_publicacao = models.DateTimeField('Data de Publicação')
    def __str__(self):
        return self.autor_comentario
    def publicada_recentemente(self):
        agora = timezone.now()
        return agora - datetime.timedelta(days=1) <= self.data_publicacao <= agora