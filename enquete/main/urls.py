from django.urls import path
from . import views

app_name ='main'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),

    path('enquete/<int:pk>', views.DetalhesView.as_view(), name='detalhes'),

    path('enquete/<int:pk>/resultado', views.ResultadoView.as_view(), name='resultado'),

    path('', views.ListarComentariosView.as_view(), name='listar_comentario'),

    path('enquete/<int:pk>/comentario', views.DetalhesComentariosView.as_view(), name='detalhes_comentario'),

    path('enquete/<int:id_enquete>/votacao', views.votacao, name='votacao'),
]