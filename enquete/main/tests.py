from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from main import models
import datetime

class PerguntaTeste(TestCase):
    def test_publicada_recentimente_com_pergunta_no_futuro(self):
        """
            O metodo publicada_recentemente precisa retornar FALSE quando
            se tratar de perguntas com data de publicação no futuro
        """
        data = timezone.now() + datetime.timedelta(seconds=1)
        pergunta_futura = models.Pergunta(data_publicacao = data)
        self.assertIs(pergunta_futura.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_anterior_a_24hs_no_passado(self):
        """
            O metodo publicada_recentemente DEVE retornar FALSE quando se tratar
            de uma data de publicação anterior a 24h no passado.
        """
        data = timezone.now() - datetime.timedelta(days = 1, seconds=1)
        pergunta_passado = models.Pergunta(data_publicacao = data)
        self.assertIs(pergunta_passado.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_nas_ultimas_24hs(self):
        """
            O metodo publicada_recentemente DEVE retornar TRUE quando se tratar de
            uma data de publicação dentro das últimas 24hs
        """
        data = timezone.now() - datetime.timedelta(hours = 23 , minutes = 59, seconds = 59)
        pergunta_ok = models.Pergunta(data_publicacao = data)
        self.assertIs(pergunta_ok.publicada_recentemente(), True)

def criar_perguta(texto, dias):
    """
        Função para criação de uma pergunta para texto e uma variação de dias.
    """
    data = timezone.now() + datetime.timedelta(days = dias)
    return models.Pergunta.objects.create( texto = texto, data_publicacao = data )

class IndexViewTeste(TestCase):
    def test_sem_perguntas_cadastradas(self):
        """
            Exibe mensagem epecífica quando não houverem perguntas cadastradas.
        """
        resposta = self.client.get( reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Nenhuma questão disponivel!!!")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'],[])

    def test_com_pergunta_no_passado(self):
        """
            Exibe normalmente pergunta no passado.
        """
        criar_perguta(texto = "Pergunta no passado", dias = -30)
        resposta = self.client.get( reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        #self.assertContains(resposta, "Nenhuma questão disponivel!!!")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'],
        ['<Pergunta: Pergunta no passado>']
        )

    def test_com_pergunta_no_futuro(self):
        """
            Perguntas com data de publicação no futuro NÃO DEVEM ser exibidas.
        """
        criar_perguta(texto = "Pergunta no futuro", dias = 1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Nenhuma questão disponivel!!!")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'],[])

    def test_pergunta_no_passado_e_no_futuro(self):
        """
            Perguntas com data de publicação no passado são exibidas e com data
            de publicação futuro são omitidas.
        """
        criar_perguta(texto = "Pergunta no passado", dias = -1)
        criar_perguta(texto = "Pergunta no futuro", dias = 1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'],
        ['<Pergunta: Pergunta no passado>']
        )

    def test_duas_perguntas_no_passado(self):
        """
            Exibie normalmente mais de uma Pergunta com data de publicação no passado são exibidas e com data
            de publicação no passado.
        """
        criar_perguta(texto = "Pergunta no passado 1", dias = -1)
        criar_perguta(texto = "Pergunta no passado 2", dias = -5)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(
        resposta.context['ultimas_perguntas'],
        ['<Pergunta: Pergunta no passado 1>',
        '<Pergunta: Pergunta no passado 2>']
        )
#================================================================================================================
class DetalhesViewTeste(TestCase):
    def test_pergunta_no_futuro(self):
        """
            Devera retornar um erro 404 ao indicar uma pergunta com data no futuro.
        """
        pergunta_futura = criar_perguta(texto = "Pergunta no futuro", dias = 5)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_futura.id])
        )
        self.assertEqual(resposta.status_code, 404)

    def test_pergunta_no_passado(self):
        """
            Devera exibir normalmente uma pergunta com data no passado.
        """
        pergunta_passada = criar_perguta(texto = "Pergunta no passado", dias = -1)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_passada.id])
        )
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, pergunta_passada.texto)






