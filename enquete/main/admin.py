from django.contrib import admin
from .models import Pergunta, Opcao, Comentario

class OpcaoInline(admin.TabularInline):
    model = Opcao
    extra = 2

class ComentarioInline(admin.TabularInline):
    model = Comentario
    extra = 1

class PerguntaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields':['texto']}),
        ('Informações de data', {'fields':['data_publicacao']}),
    ]
    inlines = [OpcaoInline,ComentarioInline]
    list_display = ('texto', 'id', 'data_publicacao', 'publicada_recentemente')
    list_filter = ['data_publicacao']
    search_fields = ['texto']

admin.site.register(Pergunta, PerguntaAdmin)
#admin.site.register(Comentario)

#admin.site.register(Opcao)
#StackedInline
#'data_publicacao', 'texto'