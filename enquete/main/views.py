from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from main import models

class IndexView(generic.ListView):
    template_name = 'main/index.html'
    context_object_name = 'ultimas_perguntas'
    def get_queryset(self):
        return models.Pergunta.objects.filter(
            data_publicacao__lte = timezone.now()
            ).order_by('-data_publicacao')[:5]

class DetalhesView(generic.DetailView):
    model = models.Pergunta
    template_name = 'main/detalhes.html'
    def get_queryset(self):
        return models.Pergunta.objects.filter(data_publicacao__lte = timezone.now())

class ListarComentariosView(generic.ListView):
    template_name = 'main/detalhes.html'
    context_object_name = 'ultimos_autores'
    def get_queryset(self):
        return models.Comentario.objects.filter(
            data_publicacao__lte = timezone.now()
            ).order_by('-data_publicacao')[:5]

class DetalhesComentariosView(generic.DetailView):
    model = models.Comentario
    template_name = 'main/detalhes_comentario.html'
    def get_queryset(self):
        return models.Comentario.objects.filter(data_publicacao__lte = timezone.now())

class ResultadoView(generic.DetailView):
    model = models.Pergunta
    template_name = 'main/resultado.html'

def votacao(request, id_enquete):
    pergunta = get_object_or_404(models.Pergunta, pk=id_enquete)
    try:
        opcao_selecionada = pergunta.opcao_set.get(pk=request.POST['opcao'])
    except(KeyError, models.Opcao.DoesNotExist):
        return render(request, 'main/detalhes.html', {
            'pergunta':pergunta,
            'error_message':"Selecione uma opção válida!"
        })
    else:
        opcao_selecionada.votos += 1
        opcao_selecionada.save()
    return HttpResponseRedirect(
        reverse('main:resultado', args=(pergunta.id,))
    )

#def index(request):
#    ultimas_perguntas = models.Pergunta.objects.order_by('-data_publicacao')[:5]
#    contexto = {'ultimas_perguntas':ultimas_perguntas}
#    return render(request, 'main/index.html', contexto)

#def detalhes(request, id_enquete):
#    pergunta = get_object_or_404(models.Pergunta, pk=id_enquete)
#    return render(request, 'main/detalhes.html', {'pergunta':pergunta})

#def resultado(request, id_enquete):
#    pergunta = get_object_or_404(models.Pergunta, pk=id_enquete)
#    return render(request, 'main/resultado.html', {'pergunta':pergunta})
